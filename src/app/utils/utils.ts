import {camelCase} from 'lodash-es';

// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
export function objectKeysToCamelCase(obj: any): any {
  if (Array.isArray(obj)) {
    return obj.map(v => objectKeysToCamelCase(v));
  } else if (obj != null && obj.constructor === Object) {
    return Object.keys(obj).reduce(
      (result, key) => ({
        ...result,
        [camelCase(key)]: objectKeysToCamelCase(obj[key]),
      }),
      {},
    );
  }
  return obj;
}
