export interface IPostResponse {
  data: IPost[];
  meta: IMeta;
}

export interface IPost {
  id: number;
  title: string;
  author: string;
  content: string;
  createdAt: Date;
}

export interface IMeta {
  currentPage: number;
  from: number;
  lastPage: number;
  path: string;
  perPage: number;
  to: number;
  total: number;
}
