import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ApiService} from './api.service';
import {map, Observable} from 'rxjs';
import {IPost, IPostResponse} from '../models/posts-response';
import {objectKeysToCamelCase} from '../utils/utils';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(
    private http: HttpClient,
    private apiService: ApiService
  ) {
  }

  getPaginate(page: number, search: string): Observable<IPostResponse> {
    const url = this.apiService.service(`/posts?page=${page}&q=${search}`);
    return this.http.get(url).pipe(
      map(res => objectKeysToCamelCase(res))
    );
  }

  find(id: string): Observable<IPost> {
    const url = this.apiService.serviceById('/posts', id);
    return this.http.get(url).pipe(
      map((res: any) => objectKeysToCamelCase(res.data))
    );
  }

  create(data: any): Observable<IPost> {
    const url = this.apiService.service('/posts');
    return this.http.post(url, data).pipe(
      map((res: any) => objectKeysToCamelCase(res.data))
    );
  }
}
