import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public urlBase: string;

  constructor() {
    this.urlBase = environment.api;
  }

  public service(service: string): string {
    return `${this.urlBase}${service}`;
  }

  public servicePaginate(service: string, page: number = 1,): string {
    return `${this.urlBase}${service}?page=${page}`;
  }

  public serviceById(service: string, id: string | number): string {
    return `${this.urlBase}${service}/${id}`;
  }
}
