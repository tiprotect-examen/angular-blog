import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PostService} from '../../../services/post.service';
import {IPost, IPostResponse} from '../../../models/posts-response';
import {debounceTime, distinctUntilChanged, fromEvent, map, Subject, takeUntil} from 'rxjs';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, OnDestroy {

  posts: IPost[] = [];
  page: number = 1;
  total: number = 0;
  from: number = 0;
  search: string = '';

  @ViewChild('filter', {static: true}) filter!: ElementRef;

  _unsubscribeAll: Subject<void> = new Subject<void>();

  constructor(
    private _postService: PostService
  ) {
  }

  ngOnInit(): void {
    this.getPosts();

    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        map((event: any) => event.target.value),
        takeUntil(this._unsubscribeAll),
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe((value) => {
        this.page = 1;
        this.search = value;
        this.getPosts();
      });
  }

  getPosts() {
    this._postService.getPaginate(this.page, this.search)
      .subscribe((res: IPostResponse) => {
        this.posts = res.data;
        this.total = res.meta.total;

      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  setCurrentPage($event: PageEvent) {
    this.page = $event.pageIndex + 1;
    this.getPosts();
  }
}
