import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PostService} from '../../../services/post.service';
import {IPost} from '../../../models/posts-response';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

  id: string;
  post?: IPost;

  constructor(
    private activatedRoute: ActivatedRoute,
    private _postService: PostService
  ) {
    this.id = activatedRoute.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    this.getPost();
  }

  getPost() {
    this._postService.find(this.id)
      .subscribe((res: IPost) => {
        this.post = res;
      });
  }
}
