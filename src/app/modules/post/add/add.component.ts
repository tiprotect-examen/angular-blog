import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PostService} from '../../../services/post.service';
import {Router} from '@angular/router';
import {IPost} from '../../../models/posts-response';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  form!: FormGroup;
  isLoading: boolean = false;

  constructor(
    private router: Router,
    private _formBuilder: FormBuilder,
    private _postService: PostService,
    private snackBar: MatSnackBar
  ) {
    this.createForm();
  }

  ngOnInit(): void {
  }

  handleSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.isLoading = true;
    this.form.disable();
    this._postService.create(this.form.getRawValue())
      .subscribe((res: IPost) => {
        this.isLoading = false;
        this.router.navigateByUrl('/posts')
          .then(() => {
            this.snackBar.open('Se ha agreado el post');
          });
      }, error => {
        this.form.enable();
        this.isLoading = false;
        this.snackBar.open('No se ha podido agregar el post.');
      });
  }

  createForm(): void {
    this.form = this._formBuilder.group({
      title: [null, [Validators.required]],
      author: [null, [Validators.required]],
      content: [null, [Validators.required]],
    });
  }
}
