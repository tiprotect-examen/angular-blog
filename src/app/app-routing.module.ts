import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'posts',
  },

  {
    path: '',
    children: [
      {
        path: 'posts',
        loadChildren: () => import('src/app/modules/post/post.module').then(m => m.PostModule)
      }
    ]
  },

  {
    path: '**',
    redirectTo: 'posts'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
